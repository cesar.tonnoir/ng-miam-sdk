import { Component } from '@angular/core';
import { GroceriesEntry } from 'projects/ng-miam/src/public_api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public ingredients: GroceriesEntry[] = [
    new GroceriesEntry('pommes de terre', '1', 'Kg'),
    new GroceriesEntry('huile d\'olive', '5', 'cL'),
    new GroceriesEntry('sel'),
  ]
  public selectedIngredients = this.ingredients;

  public isSelected(ingredient: GroceriesEntry): boolean {
    return this.selectedIngredients.includes(ingredient);
  }

  public toggleSelection(ingredient: GroceriesEntry) {
    if (this.selectedIngredients.includes(ingredient)) {
      this.selectedIngredients = this.selectedIngredients.filter(i => i != ingredient);
    } else {
      this.selectedIngredients.push(ingredient);
    }
  }
}
