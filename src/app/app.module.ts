import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgMiamModule } from 'projects/ng-miam/src/public_api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgMiamModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
