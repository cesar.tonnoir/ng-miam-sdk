import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { NgxJsonapiModule } from 'ngx-jsonapi';
import { NgMiamOrderComponent } from './_components/order/order.component';
import { GroceriesListsService } from './_services/groceries-lists.service';
import { environment } from './environment';

@NgModule({
  imports: [
    CommonModule,
    NgxJsonapiModule.forRoot({ url: [environment.miamAPI, 'api/v1/'].join('/') })
  ],
  providers: [
    GroceriesListsService
  ],
  declarations: [
    NgMiamOrderComponent
  ],
  exports: [
    NgMiamOrderComponent
  ]
})
export class NgMiamModule { }
