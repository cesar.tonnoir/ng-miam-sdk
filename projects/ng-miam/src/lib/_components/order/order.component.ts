import { Component, OnInit, Input } from '@angular/core';
import { GroceriesList } from '../../_models/groceries-list';
import { GroceriesEntry } from '../../_models/groceries-entry';
import { GroceriesListsService } from '../../_services/groceries-lists.service';
import { environment } from '../../environment';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'ng-miam-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class NgMiamOrderComponent implements OnInit {
  @Input() ingredients: GroceriesEntry[];
  @Input() locale = 'en'
  @Input() text = 'Order with Miam';
  public loading = false;

  constructor(
    private groceriesLists: GroceriesListsService
  ) { }

  ngOnInit() { }

  public order() {
    if (!this.ingredients || this.ingredients.length < 1) {
      console.error('Please pass at least one ingredient to the order component');
      return;
    }

    this.loading = true;
    this.groceriesLists.postNew(this.ingredients).pipe(
      finalize(() => this.loading = false)
    ).subscribe(
      (list: GroceriesList) => window.open(this.url(list.id))
    );
  }

  private url(id): string {
    let urlMembers = [environment.miamWeb];
    if(!environment.development) urlMembers.push(this.locale);
    urlMembers.push(`app/groceries-lists/${id}`);
    
    return urlMembers.join('/');
  }

}
