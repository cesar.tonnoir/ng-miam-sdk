import { Resource } from "ngx-jsonapi";

export class GroceriesEntry extends Resource {
  public attributes = {
    'name': '',
    'capacity-unit': '',
    'capacity-volume': '',
    'capacity-factor': '',
  };

  constructor(name, unit = '', volume = '1', factor = '1') {
    super();
    this.attributes.name = name;
    this.attributes['capacity-unit'] = unit;
    this.attributes['capacity-volume'] = volume;
    this.attributes['capacity-factor'] = factor;
  }
}