import { Resource, DocumentCollection } from "ngx-jsonapi";
import { GroceriesEntry } from "./groceries-entry";

export class GroceriesList extends Resource {
  public attributes = {
    'name': '',
    'created-at': '',
    'updated-at': '',
    'entries': []
  }

  public relationships = {
    'groceries-entries': new DocumentCollection<GroceriesEntry>()
  };
}