import { Injectable } from '@angular/core';
import { GroceriesList } from '../_models/groceries-list';
import { Autoregister, Service, DocumentResource } from 'ngx-jsonapi';
import { GroceriesEntry } from '../_models/groceries-entry';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
@Autoregister()
export class GroceriesListsService extends Service<GroceriesList> {
  public resource = GroceriesList;
  public type = 'groceries-lists';

  public postNew(entries: GroceriesEntry[]): Observable<GroceriesList> {
    const list = this.new();
    list.is_loading = false;
    delete(list.id);
    delete(list.relationships["groceries-entries"]);
    
    list.attributes.entries = entries
      .map(e => e.attributes)
      .filter(e => !!e.name);

    return list.save().pipe(
      map((l: DocumentResource<GroceriesList>) => l.data)
    );
  }
}
