# NgMiamSdk

NgMiam SDK is an open-source library facilitating interactions between the groceries ordering service **[Miam](https://miam.tech)** and any Angular application.

This project was generated with Angular CLI version 7.3.4.

**Features list**

- ["Order with Miam" button](#order-with-miam-button)

## Getting started

Install the library:

```
npm install ng-miam
```

Import NgMiamModule to your AppModule:

```typescript
import { NgMiamModule } from 'ng-miam';

@NgModule({
  imports: [
    NgMiamModule
  ]
})
```

## Features

### Order with Miam button

Use the `GroceriesEntry` model to define ingredients:

```typescript
import { GroceriesEntry } from 'ng-miam';
@Component({})
export class AppComponent {
  public ingredients: GroceriesEntry[] = [
    new GroceriesEntry('pommes de terre', '1', 'Kg'),
    new GroceriesEntry('huile d\'olive', '5', 'cL'),
    new GroceriesEntry('sel'),
  ]
}
```

Use the `ng-miam-order` component to inject an "Order with Miam" button to your template:

```html
<ng-miam-order
  [ingredients]="ingredients"
  locale="en"
  text="Order with Miam"
></ng-miam-order>
```

**You're all set!** Clicking the button will send the ingredients list to Miam, and prepare a groceries basket for you.

## Running the demo application

To help you getting started, a demo application is available under `/src/app`. You can run it locally like any angular app.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
